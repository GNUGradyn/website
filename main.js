particlesJS.load('particles', 'particles.json', function() {
    console.log('particles.js loaded succsessfuly');
  });

  $(document).ready(function(){
    $("a").on('click', function(event) {
        if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){
          window.location.hash = hash;
        });
      }
    });
  });

function KonamiMode() {
    document.getElementById("konami-note").style.display = "block"
    console.log("Konami Mode Activated")
  }
function closenote() {
  document.getElementById("konami-note").style.display = "none"
  document.getElementById("konami").style.display = "block"
}
function opensettings() {
  document.getElementById("settings").style.display = "block"
}
var easter_egg = new Konami(function() {KonamiMode()});